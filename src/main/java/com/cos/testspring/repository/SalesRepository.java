package com.cos.testspring.repository;

import com.cos.testspring.model.Sales;
import com.cos.testspring.web.dto.SalesInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface SalesRepository extends JpaRepository<Sales,Integer> {
   @Query(value="SELECT DATE_FORMAT(date,'%Y-%m') 'date',name,SUM(quantity) quantity FROM sales WHERE DATE_FORMAT(date,'%Y-%m')"+
   " =DATE_FORMAT(NOW(),:date) GROUP BY 'date',name,'quantity' ORDER BY 2 desc;",nativeQuery=true)
   List<SalesInterface> findByMonth(@Param("date") String date);

   @Query(value="SELECT DATE_FORMAT(date,'%Y-%m-%d') 'date',name,quantity FROM sales WHERE DATE_FORMAT(date,'%Y-%m-%d') "+
   "=DATE_FORMAT(NOW(),:date) GROUP BY 'date',name,'quantity' ORDER BY 2 desc;",nativeQuery=true)
   List<SalesInterface> findByDay(@Param("date") String date);

   @Query(value="SELECT DATE_FORMAT(date,'%Y-%m') 'date',name,RANK() OVER (ORDER BY SUM(quantity) DESC) as quantity FROM"+
   " sales WHERE DATE_FORMAT(date,'%Y-%m') =DATE_FORMAT(NOW(),:date) GROUP BY 'date',name,'quantity' ORDER BY 2 desc;",nativeQuery=true)
   List<SalesInterface> findByMonthRank(@Param("date") String date);

   @Query(value="SELECT DATE_FORMAT(DATE,'%Y-%m-%d') AS 'date',NAME,quantity FROM testspring.sales WHERE DATE_FORMAT(date,'%Y-%m-%d')"+
   " >=DATE_FORMAT(NOW(),:date1) and DATE_FORMAT(date,'%Y-%m-%d') <=DATE_FORMAT(NOW(),:date2) ORDER BY 1,2;",nativeQuery=true)
   List<SalesInterface> findByDate(@Param("date1") String date1,@Param("date2") String date2);

   Sales findByNameAndDate(String name,LocalDate date);
}
