package com.cos.testspring.repository;




import org.springframework.data.jpa.repository.JpaRepository;


import com.cos.testspring.model.Token;

public interface TokenRepository extends JpaRepository<Token,Integer> {

//    @Modifying
//    @Query(value="INSERT INTO user(username,password) VALUES (:username,:password);",nativeQuery=true)
//    void mSave(@Param("username") String username, @Param("password") String password);


//    @Modifying
//    @Query(value="SELECT email FROM user WHERE (:email);",nativeQuery=true)
//    User mFind(@Param("email") String email);

    public Token findByRefreshToken(String refreshToken);
    public Token findByAccessToken(String accessToken);
    public Token findById(int id);

}
