package com.cos.testspring.repository;

import com.cos.testspring.model.Board;
import com.cos.testspring.model.Hydrogen;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HydrogenRepository extends JpaRepository<Hydrogen,String> {

}