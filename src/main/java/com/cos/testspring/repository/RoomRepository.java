package com.cos.testspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cos.testspring.model.Room;

//JpaRepository를 상속했으면 어노테이션이 없어도 Ioc등록이 자동으로된다.
public interface RoomRepository extends JpaRepository<Room,Integer> {

	@Modifying
	@Query(value="SELECT * FROM room r WHERE r.id IN(SELECT c.roomId FROM chat c WHERE userEmail=:userEmail GROUP BY roomId);",nativeQuery=true)
	List<Room> mRoomId(@Param("userEmail") String userEmail);
    
    Room findById(int roomId);

	Room findByRoomName(String roomName);
}
