package com.cos.testspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cos.testspring.model.Chat;



//JpaRepository를 상속했으면 어노테이션이 없어도 Ioc등록이 자동으로된다.
public interface ChatRepository extends JpaRepository<Chat,Integer> {


}
