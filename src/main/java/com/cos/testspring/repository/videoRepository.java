package com.cos.testspring.repository;

import com.cos.testspring.model.video;

import org.springframework.data.jpa.repository.JpaRepository;

public interface videoRepository extends JpaRepository<video,Integer> {

}
