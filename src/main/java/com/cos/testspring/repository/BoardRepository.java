package com.cos.testspring.repository;

import com.cos.testspring.model.Board;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRepository extends JpaRepository<Board,Integer> {

//    @Modifying
//    @Query(value="INSERT INTO user(username,password) VALUES (:username,:password);",nativeQuery=true)
//    void mSave(@Param("username") String username, @Param("password") String password);

//    public User findByUsername(String Username);

    // @Query(value="select * from board",nativeQuery=true)
    // Page<Board> mRecent(Pageable pageable);S
    
    Page<Board> findByclassification(String classification, Pageable pageable);
}