package com.cos.testspring.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.cos.testspring.model.Hydrogen;
import com.cos.testspring.repository.HydrogenRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
@RequiredArgsConstructor
@Service
public class SchedulerService {
    
    private final HydrogenRepository hydrogenRepository;

    @Scheduled(fixedDelay = 60 * 1000) // 1분마다 실행 1초 = 1000
    public void run() {
        // BufferedReader in = null;
        // ObjectMapper objectMapper = new ObjectMapper();
        // try {
        //     if(hydrogenRepository.count()<1){
        //         URL obj = new URL("http://el.h2nbiz.or.kr/api/chrstnList/operationInfo"); // 호출할 url
        //         HttpURLConnection con = (HttpURLConnection)obj.openConnection();
     
        //         con.setRequestMethod("GET");
        //         con.setRequestProperty("Authorization", "sMVYBl1av5YMSl3m0COY3VL87X3SalmUTf2qrVqlJEd0aQVfwXGFUVz3q414RD27");
     
        //         in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                
        //         JSONArray jArray = new JSONArray(in.readLine());
                
        //         for(int i =0;i<jArray.length();i++){
        //             Hydrogen objectUser = objectMapper.readValue(jArray.get(i).toString(), Hydrogen.class);
        //             hydrogenRepository.save(objectUser);
        //         }
        //     }
        //     URL obj = new URL("http://el.h2nbiz.or.kr/api/chrstnList/currentInfo"); // 호출할 url
        //         HttpURLConnection con = (HttpURLConnection)obj.openConnection();
     
        //         con.setRequestMethod("GET");
        //         con.setRequestProperty("Authorization", "sMVYBl1av5YMSl3m0COY3VL87X3SalmUTf2qrVqlJEd0aQVfwXGFUVz3q414RD27");
     
        //         in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
    
        //         JSONArray jArray = new JSONArray(in.readLine());
                
        //         for(int i =0;i<jArray.length();i++){
                    
        //             Hydrogen newHydrogen = objectMapper.readValue(jArray.get(i).toString(), Hydrogen.class);
        //             Hydrogen hydrogen = hydrogenRepository.findById(newHydrogen.getChrstn_mno()).orElseThrow();
        //             hydrogen.set(newHydrogen);
        //             hydrogenRepository.save(hydrogen);
        //         }
            
        // } catch(Exception e) {
        //     e.printStackTrace();
        // } finally {
        //     if(in != null) try { in.close(); } catch(Exception e) { e.printStackTrace(); }
        // }
    }
}
