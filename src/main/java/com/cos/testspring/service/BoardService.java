package com.cos.testspring.service;

import com.cos.testspring.model.Board;
import com.cos.testspring.config.constants.Constants;
import com.cos.testspring.model.User;
import com.cos.testspring.model.file;
import com.cos.testspring.model.img;
import com.cos.testspring.model.video;
import com.cos.testspring.repository.BoardRepository;
import com.cos.testspring.repository.ImgRepository;
import com.cos.testspring.repository.fileRepository;
import com.cos.testspring.repository.videoRepository;
import com.cos.testspring.web.dto.BoardDto;
import lombok.RequiredArgsConstructor;

import org.apache.poi.ss.usermodel.Row;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class BoardService extends com.cos.testspring.service.Service<Board>{
    public Constants constants;
    private final BoardRepository boardRepository;
    private final ImgRepository imgRepository;
    private final videoRepository videoRepository;
    private final fileRepository fileRepository;
    
    
     public Board insertPosting(BoardDto boardDto,User user){
        List<String> imgNames =null;
        List<String> fileNames =null;
        List<String> videoNames =null;
        int i=1;
        
        if(boardDto.getFile()!= null ){
        if(boardDto.getFile().get(0).getSize()>0){
            fileNames=new ArrayList<>();
            for(MultipartFile imgFile : boardDto.getFile()){
                UUID uuid = UUID.randomUUID();
                String BoardFileName= uuid+"_"+imgFile.getOriginalFilename();
                Path BoardFilePath = Paths.get(Constants.IMG_PATH+BoardFileName);
                try {
                    // uuid 사용자가 동일 1.jpg라는 사진을 업로드할 경우 덮어씌어지므로
                    Files.write(BoardFilePath, imgFile.getBytes()); 
                    //이미지가 업로드되다가 여기서 실패를 하면 익셉션 처리 해줘야함
                 }catch(Exception e) {
                  System.out.print(i+"번째 파일 업로드 실패");
                    e.printStackTrace();
                 }
                 fileNames.add(BoardFileName);
                 i++;
            }
        }}
        i=1;
        if(boardDto.getImg()!= null){
        if(boardDto.getImg().get(0).getSize()>0){
            imgNames=new ArrayList<>();
            for(MultipartFile imgFile : boardDto.getImg()){
                UUID uuid = UUID.randomUUID();
                String BoardFileName= uuid+"_"+imgFile.getOriginalFilename();
                Path BoardFilePath = Paths.get(Constants.IMG_PATH+BoardFileName);
                try {
                    Files.write(BoardFilePath, imgFile.getBytes()); 
                }catch(Exception e) {
                System.out.print(i+"번째 이미지 업로드 실패");
                    e.printStackTrace();
             }
             imgNames.add(BoardFileName);
             i++;
        }}
        }
        i=1;
        if(boardDto.getVideo()!= null){
        if(boardDto.getVideo().get(0).getSize()>0){
            videoNames=new ArrayList<>();
            for(MultipartFile imgFile : boardDto.getVideo()){
                UUID uuid = UUID.randomUUID();
                String BoardFileName= uuid+"_"+imgFile.getOriginalFilename();
                Path BoardFilePath = Paths.get(Constants.IMG_PATH+BoardFileName);
                try {
                    Files.write(BoardFilePath, imgFile.getBytes()); 
                }catch(Exception e) {
                System.out.print(i+"번째 비디오 업로드 실패");
                    e.printStackTrace();
                }
                videoNames.add(BoardFileName);
                i++;
            }
        }}
 
        Board board = boardDto.toEntity(boardDto.getClassification(),user.getId());
         
        boardRepository.save(board);
        if(imgNames != null){
            for(String BoardFileName :imgNames){
                img img =new img();
                img.setBoard(board);
                img.setName(BoardFileName);
                imgRepository.save(img);
            }
        }
        if(fileNames != null){
            for(String BoardFileName :fileNames){
                file file =new file();
                file.setBoard(board);
                file.setName(BoardFileName);
                fileRepository.save(file);
            }
        }
        if(videoNames != null){
            for(String BoardFileName :videoNames){
                video video =new video();
                video.setBoard(board);
                video.setName(BoardFileName);
                videoRepository.save(video);
            }
        }
        
        return board;
     }
    /*=========글 작성 INSERT(파일업로드 없이)=========*/
    public void  NotFilePosting(BoardDto boardDto){

     boardDto.setTitle(boardDto.getTitle());
     boardDto.setContent(boardDto.getContent());
     boardDto.setFile(null);
     Board board = boardDto.toEntity(boardDto.getClassification(),0);
     boardRepository.save(board);

    }

    /*==========게시판 불러오기===============*/
    public Page<Board> getBoard(Pageable pageable){

    	Page<Board> board = boardRepository.findAll(pageable);

        return board;
    }
    public Page<Board> getBoard(Pageable pageable,String classification){

    	//Page<Board> board = boardRepository.findByclassificationOrderByIdDesc(classification,pageable);
        Page<Board> board =boardRepository.findByclassification(classification, pageable);
        return board;
    }

    @Override
    public int[] getColumn(Row row){
        int[] index = new int[4];
        for(int i =0;i<4;i++){
            switch(row.getCell(i).getStringCellValue()){
                case "title":
                index[0]=i;
                    break; 
                case "content":
                index[1]=i;
                    break; 
                case "classification":
                index[2]=i;
                    break; 
                case "userId":
                index[3]=i;
                    break; 
                default: 
                index[i]=4;
                    break;
            }
        }
        return index;
    }

    @Override
    public Board save(Row row,int[] index){
        Board board = new Board(
            row.getCell(index[0]).getStringCellValue(),
            row.getCell(index[1]).getStringCellValue(),
            row.getCell(index[2]).getStringCellValue(),
            (int)row.getCell(index[3]).getNumericCellValue());
            return board;
    }
}
