package com.cos.testspring.service;


import com.cos.testspring.config.constants.Constants;
import com.cos.testspring.model.User;

import com.cos.testspring.repository.UserRepository;

import lombok.RequiredArgsConstructor;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@RequiredArgsConstructor
@Service
public class UserService {
    public Constants constants;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserRepository userRepository;
    
    public void addDummyUser(){
        
        User admin = new User();
        User manager = new User();
        
        admin.setRole("ROLE_ADMIN");
        manager.setRole("ROLE_MANAGER");

        String rawPassword = "123123123";
        String encPassword = bCryptPasswordEncoder.encode(rawPassword);

        
        admin.setUsername("admin");
        manager.setUsername("manager");

        
        admin.setPassword(encPassword);
        manager.setPassword(encPassword);
        
        admin.setEmail("admin@123.com");
        manager.setEmail("manager@123.com");

        admin.setProfil("1.jpg");
        manager.setProfil("2.jpg");

        admin.setPhone("01011111111");
        manager.setPhone("01022222222");
        userRepository.save(admin);
        userRepository.save(manager);
    }

}
