package com.cos.testspring.service;

import com.cos.testspring.model.Sales;
import com.cos.testspring.repository.SalesRepository;
import com.cos.testspring.web.dto.SalesDto;
import com.cos.testspring.web.dto.SalesInterface;
import lombok.RequiredArgsConstructor;

import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class SalesService extends com.cos.testspring.service.Service<Sales>{
    private final SalesRepository salesRepository;

    public List<SalesDto> comparison(String date) {//받은 날짜의 전월과 어번달 매출 비교
        List<SalesInterface> targetValueSales = null;
        List<SalesInterface> quantitySales = null;
        try {
            quantitySales = salesRepository.findByMonth(date);
            int month = Integer.parseInt(date.split("-")[1]);
            int year = Integer.parseInt(date.split("-")[0]);
            if (month == 1) {
                month = 12;
                year = year - 1;
            } else {
                month = month - 1;
            }
            if (month < 10) {
                date = year + "-0" + month;
            } else {
                date = year + "-" + month;
            }
            targetValueSales = salesRepository.findByMonth(date);
        } catch (Exception e) {
            return null;
        }
        List<SalesDto> salesDtoList = new ArrayList<>();
        for (int i = 0; i < targetValueSales.size(); i++) {
            SalesDto salesDto = new SalesDto(targetValueSales.get(i).getDate(), quantitySales.get(i).getQuantity(),
                    targetValueSales.get(i).getName(), targetValueSales.get(i).getQuantity(), 0);
            salesDtoList.add(i, salesDto);
        }
        return salesDtoList;
    }

    public List<SalesDto> ratio(String date) {
        List<SalesInterface> SalesInterface = null;
        List<SalesDto> salesDtoList =new ArrayList();
        double totalQuantity = 0;
        try {
            SalesInterface = salesRepository.findByMonth(date);
            for (int i = 0; i < SalesInterface.size(); i++) {
                totalQuantity += SalesInterface.get(i).getQuantity();
            }
            for (int i = 0; i < SalesInterface.size(); i++) {
                SalesDto salesDto = new SalesDto();
                salesDto.setId(SalesInterface.get(i).getName());
                salesDto.setValue(Math.round((SalesInterface.get(i).getQuantity() / totalQuantity) * 1000) / 10.0);
                salesDtoList.add(salesDto);
            }
        } catch (Exception e) {
            return null;
        }
        return salesDtoList;
    }

    public List<JSONObject> selectByrange(String startDate, String endDate) {
        List<SalesInterface> SalesInterface = null;
        //받은 날짜를 년,월로 나누고
        int startYear = Integer.parseInt(startDate.split("-")[0]);
        int startMonth = Integer.parseInt(startDate.split("-")[1]);
        int endYear = Integer.parseInt(startDate.split("-")[0]);
        int endMonth = Integer.parseInt(endDate.split("-")[1]);
        int month = 0;
        String start = "";
        //어떤 날짜가 최근인지 찾아
        if (startYear == endYear) {
            if (startMonth > endMonth) {
                month = startMonth - endMonth;
                start = endDate;
            } else if (startMonth < endMonth) {
                month = endMonth - startMonth;
                start = startDate;
            } else {
                return null;
            }
        } else if (startYear > endYear) {
            int Year = startYear - endYear;
            month = (startMonth + 12 * Year) - endMonth;
            start = endDate;
        } else if (startYear < endYear) {
            int Year = endYear - startYear;
            month = (endMonth + 12 * Year) - startMonth;
            start = startDate;
        }
        //그 차이만큼 데이터를 가져옴
        List<JSONObject> jsonObjectList = new ArrayList();
        startYear = Integer.parseInt(start.split("-")[0]);
        startMonth = Integer.parseInt(start.split("-")[1]) - 1;
        for (int i = 0; i <= month; i++) {
            if (startMonth == 12) {
                startMonth = 1;
                startYear = startYear + 1;
            } else {
                startMonth = startMonth + 1;
            }
            if (startMonth < 10) {
                start = startYear + "-0" + startMonth;
            } else {
                start = startYear + "-" + startMonth;
            }
            try {
                SalesInterface = salesRepository.findByMonth(start);
            } catch (Exception e) {
                return null;
            }
            JSONObject jsonObject = new JSONObject();
            for (int j = 0; j < SalesInterface.size(); j++) {
                if (j == 0) {
                    jsonObject.put("country", startMonth + "월");
                }
                jsonObject.put(SalesInterface.get(j).getName(), SalesInterface.get(j).getQuantity());
            }
            jsonObjectList.add(jsonObject);
        }
        return jsonObjectList;
    }

    public List<JSONObject> selectByrangeRank(String startDate, String endDate) {
        List<SalesInterface> totalSalesInterface = new ArrayList<>();
        int startYear = Integer.parseInt(startDate.split("-")[0]);
        int startMonth = Integer.parseInt(startDate.split("-")[1]);
        int endYear = Integer.parseInt(startDate.split("-")[0]);
        int endMonth = Integer.parseInt(endDate.split("-")[1]);
        int month = 0;
        int nameSize = 0;
        String start = "";
        //어떤 날짜가 최근인지 찾아
        if (startYear == endYear) {
            if (startMonth > endMonth) {
                month = startMonth - endMonth;
                start = endDate;
            } else if (startMonth < endMonth) {
                month = endMonth - startMonth; // 4
                start = startDate;// 2022-08
            } else {
                return null;
            }
        } else if (startYear > endYear) {
            int Year = startYear - endYear;
            month = (startMonth + 12 * Year) - endMonth;
            start = endDate;
        } else if (startYear < endYear) {
            int Year = endYear - startYear;
            month = (endMonth + 12 * Year) - startMonth;
            start = startDate;
        }
        //그 차이만큼 데이터를 가져옴
        List<JSONObject> jsonObjectList = new ArrayList();
        startYear = Integer.parseInt(start.split("-")[0]);
        startMonth = Integer.parseInt(start.split("-")[1]);
        for (int i = 0; i <= month; i++) {
            List<SalesInterface> SalesInterface = null;

            if (startMonth < 10) {
                start = startYear + "-0" + startMonth;
            } else {
                start = startYear + "-" + startMonth;
            }
            try {
                SalesInterface = salesRepository.findByMonthRank(start);
            } catch (Exception e) {
                return null;
            }
            totalSalesInterface.addAll(SalesInterface);
            nameSize = SalesInterface.size();
            if (startMonth == 12) {
                startMonth = 1;
                startYear = startYear + 1;
            } else {
                startMonth = startMonth + 1;
            }
        }
        //가져온 데이터를 그래프 템플릿에 맞게 데이터를 가공해줌
        for (int j = 0; j < nameSize; j++) {
            JSONObject totalJsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            for (int i = j; i < totalSalesInterface.size(); i += nameSize) {
                JSONObject jsonObject = new JSONObject();
                if (i < nameSize) {
                    totalJsonObject.put("id", totalSalesInterface.get(i).getName());
                }
                jsonObject.put("x", Integer.parseInt(totalSalesInterface.get(i).getDate().split("-")[1]) + "월");
                jsonObject.put("y", totalSalesInterface.get(i).getQuantity());
                jsonArray.add(jsonObject);
            }
            totalJsonObject.put("data", jsonArray);
            jsonObjectList.add(totalJsonObject);
        }
        return jsonObjectList;
    }
    public void addSales1(){//2022-07-01~2022-12-16동안에 매출 데이터 생성
        String name = "";
        for (int i = 0; i < 4; i++) {// 각음료마다 1번씩 총 4번
            name = "";
            if (i == 0) {
                name = "코카콜라";
            } else if (i == 1) {
                name = "칠성사이다";
            } else if (i == 2) {
                name = "환타";
            } else {
                name = "토레타";
            }
            for (int ii = 1; ii < 4; ii++) {// 다른 일수다마 한번씩총 3번
                for (int iii = 0; iii < ii; iii++) {// 그 일수에 따른 달마다 한번씩 총6번
                    int day = 0;
                    int m = 0;
                    if (ii == 1) {
                        day = 16;
                    } else if (ii == 2) {
                        day = 30;
                    } else {
                        day = 31;
                    }
                    if (day == 16) {
                        m = 12;
                    } else if (day == 30 && iii == 0) {
                        m = 9;
                    } else if (day == 30 && iii == 1) {
                        m = 11;
                    } else if (day == 31 && iii == 0) {
                        m = 7;
                    } else if (day == 31 && iii == 1) {
                        m = 8;
                    } else if (day == 31 && iii == 2) {
                        m = 10;
                    }
                    for (int jj = 1; jj < day + 1; jj++) {
                        int quantity = (int) (Math.random() * 4000 + 1) + 3000;// 3000~7000랜덤수
                        LocalDate date = LocalDate.of(2022, m, jj);
                        Sales sales = new Sales(name, quantity, date);
                        salesRepository.save(sales);
                    }
                }
            }
        }
    }
    


    public List<JSONObject> selectByDayList(String startDate, String endDate){
        List<JSONObject> JSONObjectList= new ArrayList();
        try{
            if(Integer.parseInt(startDate.replace("-", ""))>
            Integer.parseInt(endDate.replace("-", ""))){
                String temp=startDate;
                startDate=endDate;
                endDate=temp;
            }
            List<SalesInterface> list = salesRepository.findByDate(startDate, endDate);
            for(int i=0;i<list.size()/4;i++){
                JSONObject totalJsonObject = new JSONObject();
                JSONArray JsonArrayDate = new JSONArray();
                for(int j=0;j<4;j++){
                    JSONObject JsonObject = new JSONObject();
                    
                    JsonObject.put("quantity", list.get(j+(i*4)).getQuantity());
                    JsonObject.put("name", list.get(j+(i*4)).getName());
                    
                    JsonArrayDate.add(JsonObject);
                }
                totalJsonObject.put("date", list.get(i*4).getDate());
                totalJsonObject.put("value", JsonArrayDate);
                JSONObjectList.add(totalJsonObject);
            }
        }catch(Exception e){
            return null;
        }
        return JSONObjectList;
    }

    public File downloadExcel(String startDate, String endDate) throws IOException{
        if(Integer.parseInt(startDate.replace("-", ""))>
        Integer.parseInt(endDate.replace("-", ""))){
            String temp=startDate;
            startDate=endDate;
            endDate=temp;
        }
        try (Workbook workbook = new XSSFWorkbook()) {// .xlsx 파일 생성
            Sheet sheet = workbook.createSheet("판매량 정리");//시트 생성및 이름지정
            int rowNo = 0; //행 번호 
 
            CellStyle headStyle = workbook.createCellStyle(); // 첫번째 행 스타일 지정
            headStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.LIGHT_BLUE.getIndex());
            headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            Font font = workbook.createFont();//첫번째 행 글자 스타일 지정
            font.setColor(HSSFColor.HSSFColorPredefined.WHITE.getIndex());
            font.setFontHeightInPoints((short) 13);
            headStyle.setFont(font);
 
            Row headerRow = sheet.createRow(rowNo++); //첫번째 행 데이터 입력
            headerRow.createCell(0).setCellValue("date");
            headerRow.createCell(1).setCellValue("name");
            headerRow.createCell(2).setCellValue("quantity");
            
            for(int i=0; i<=2; i++){//스타일 적용
                headerRow.getCell(i).setCellStyle(headStyle);
            }
            //db데이터 자동 입력
            List<SalesInterface> list = salesRepository.findByDate(startDate,endDate);
            for (SalesInterface sales : list) {
                Row row = sheet.createRow(rowNo++);
                row.createCell(0).setCellValue(sales.getDate());
                row.createCell(1).setCellValue(sales.getName());
                row.createCell(2).setCellValue(sales.getQuantity());
            }
            //각컬럼 사이즈 지정
            // sheet.setColumnWidth(0, 3000);
            // sheet.setColumnWidth(1, 3000);
            // sheet.setColumnWidth(2, 4000);
 
            //Temp파일 생성 후 만든 엑셀파일을 저장하고 리턴해준다.
            File tmpFile = File.createTempFile("TMP~", ".xlsx");
            try (OutputStream fos = new FileOutputStream(tmpFile);) {
                workbook.write(fos);
            }
            return tmpFile;
    }
}
        
    @Override
    public int[] getColumn(Row row){
        int[] index = new int[3];

        for(int i =0;i<3;i++){

            switch(row.getCell(i).getStringCellValue()){   

                case "name":
                    index[0]=i;
                    break; 

                case "quantity":
                    index[1]=i;
                    break; 

                case "date":
                    index[2]=i;
                    break;  

                default: 
                    index[i]=3;
                    break;
            }
        }
        return index;
    }

    @Override
    public Sales save(Row row,int[] index){
        String[] date =row.getCell(index[2]).getStringCellValue().split("-");
        LocalDate localDate = LocalDate.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2]));
        Sales sales =salesRepository.findByNameAndDate(row.getCell(index[0]).getStringCellValue(), localDate);
        if(sales == null){
            sales = new Sales(
                row.getCell(index[0]).getStringCellValue(),
                (int)row.getCell(index[1]).getNumericCellValue(),
                localDate);
        }else{
            sales.setQuantity((int)row.getCell(index[1]).getNumericCellValue());
        }
        return sales;
    }
    public List<SalesDto> SalesDtoList(List<SalesInterface> salesList){
        List<SalesDto> salesDtos = new ArrayList<>();
        for(SalesInterface sales : salesList){
            SalesDto salesDto = new SalesDto();
            salesDto.setQuantity(sales.getQuantity());
            salesDto.setDate(sales.getDate().toString());
            salesDto.setName(sales.getName());
            salesDtos.add(salesDto);
        }
        return salesDtos;
    }
}
