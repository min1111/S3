package com.cos.testspring.config;

import com.cos.testspring.config.handler.ex.ExceptionHandlerFilter;
import com.cos.testspring.config.jwt.JwtAuthenticationFilter;
import com.cos.testspring.config.jwt.JwtAuthorizationFilter;
import com.cos.testspring.repository.TokenRepository;
import com.cos.testspring.repository.UserRepository;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.RequiredArgsConstructor;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.filter.CorsFilter;

//http://localhost:8080/login은 formLogin을 disable해놔서 동작안함
@RequiredArgsConstructor
@EnableWebSecurity // 3.현재 이 파일로 시큐리티를 활성화
@Configuration // 2.Ioc에 띄워줘야함 !!
@EnableGlobalMethodSecurity(securedEnabled = true) // Secured 어노테이션 활성화
public class SecurityConfig extends WebSecurityConfigurerAdapter{ // 1.WebSecurityConfigurerAdapter로 상속해줘야함

	private final CorsFilter corsFilter;

	private final UserRepository userRepository;

	private final TokenRepository tokenRepository;

	@Bean
	public BCryptPasswordEncoder encodedPwd() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		//http.addFilter(new MyFilter1()); 시큐리티에서는 이렇게 걸면안됨
		//http.addFilterBefore(new MyFilter3(),BasicAuthenticationFilter.class); //시큐리티의 특정필터가 실행되기 이전에 필터가 실행되어라.

		http.cors().configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues()); // /login CORS 허용
		http.addFilterBefore(//JwtAuthorizationFilter에서 오류 발생시 ExceptionHandlerFilter에서 처리를 해준다.
				new ExceptionHandlerFilter(),
				JwtAuthorizationFilter.class
		);
		http.csrf().disable();
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // 세션을 쓰지 않겠다.
		.and()
		.addFilter(corsFilter) // CorsConfig.java 에 있는 corsFilter 설정 =>
				// 이제 CrossOrigin요청이 오면 다허용함, 인증이 있을 때는 이렇게 시큐리티 필터에 걸어주어야함.
		.formLogin().disable() // 폼로그인 X
		.httpBasic().disable()
				// 기본적인 http 방식을 아에안쓰겠다.
		.addFilter(new JwtAuthenticationFilter(authenticationManager()))
				// 파람으로 AuthenticationManager를 던져줘야한다, WebSecurityConfigurerAdapter
				// 얘가 가지고 있어서authenticationManager() 라고 그냥 써주면됨
		.addFilter(new JwtAuthorizationFilter(authenticationManager(),userRepository,tokenRepository))
		.authorizeRequests()
		.requestMatchers(CorsUtils::isPreFlightRequest).permitAll() // /login CORS 허용
 		.antMatchers("/api/v1/user/**") // 이쪽으로 주소가 들어오면
		.access("hasRole('ROLE_USER') OR hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
		.antMatchers("/api/v1/manager/**") // 이쪽으로 주소가 들어오면
		.access("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
		.antMatchers("/api/v1/admin/**") // 이쪽으로 주소가 들어오면
		.access("hasRole('ROLE_ADMIN')")
		.anyRequest().permitAll();
		// 그 외는 권한없이 들어가겠다.
	}

	@Override
	public void configure(WebSecurity web) throws Exception{
		web.ignoring()
				.mvcMatchers("/join","/message/**","/exception","/createToken/**","/logout","/img/**","/excel/**","/startDemo");
	}
}

//http.csrf().disable();
//http.cors().configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues()); // /login CORS 허용
//	http.authorizeRequests()
//			.requestMatchers(CorsUtils::isPreFlightRequest).permitAll() // /login CORS 허용
//			.antMatchers("/").authenticated() // 인증(로그인) 되어야지만 들어갈 수 있는 주소
//			.anyRequest().permitAll()
//			.and()
//			.formLogin() //antMatchers에 걸리면 우리는 formLogin()을 하겠다. formLogin()이란 form태그, input태그가 있는 페이지를 말함
//			.loginProcessingUrl("/login") // 로그인 진행
//			.usernameParameter("email")
//			.passwordParameter("password")
//			.defaultSuccessUrl("/");