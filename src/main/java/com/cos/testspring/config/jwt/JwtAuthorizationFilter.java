package com.cos.testspring.config.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.cos.testspring.config.auth.PrincipalDetails;
import com.cos.testspring.config.constants.Constants;
import com.cos.testspring.model.Token;
import com.cos.testspring.model.User;
import com.cos.testspring.repository.TokenRepository;
import com.cos.testspring.repository.UserRepository;
import com.cos.testspring.web.dto.loginBody;

import org.springframework.security.core.Authentication;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

// 시큐리티가 filter가지고 있는데 그 필터중에 BasicAuthenticationFilter라는 것이 있음.
// 권한이나 인증이 필요한 특정 주소를 요청했을 때 위 필터를 무조건 타게 되어있음.
// 만약에 권한이나 인증이 필요한 주소가 아니라면 이 필터를 안타요

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {
    public Constants constants;
    private UserRepository userRepository;

    private TokenRepository tokenRepository;

    public loginBody loginBody = new loginBody();

    private static final String KEY = "cos";

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager, UserRepository userRepository,
            TokenRepository tokenRepository) {
        super(authenticationManager);
        System.out.println("인증이나 권한이 필요한 주소로 요청됨");
        this.userRepository = userRepository;
        this.tokenRepository = tokenRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        Constants.OK = false;

        // super.doFilterInternal(request,response,chain);
        System.out.println("인증이나 권한이 필요한 주소 요청이 됨11");
        // apiController.setNewTokenCheck(true);
        String jwtHeader = request.getHeader("Authorization");
        System.out.println("jwtHeader:" + jwtHeader);

        // JWT 토큰을 검증을 해서 정상적인 사용자인지 확인
        if (jwtHeader == null || !jwtHeader.startsWith("Bearer")) { // header에 토큰이 없거나 & Bearer로 시작하지 않으면
            chain.doFilter(request, response);
            return;
        }
        // JWT토큰을 검증을 해서 정상적인 사용자인지 확인
        String accessToken = request.getHeader("Authorization");
        Token token = tokenRepository.findByAccessToken(accessToken);
        Cookie[] myCookie = request.getCookies();

        Cookie refreshTokenCookie = null;
        // 쿠키에서 refresh를 꺼내고 검증을 해주고 검증성공시 access발급.
        for (Cookie cookie : myCookie) {
            if (cookie.getName().equals("refreshToken")) {
                refreshTokenCookie = cookie;
            }
        }
        if (refreshTokenCookie == null) {
            chain.doFilter(request, response);
            return;
        } else if (token.getRefreshToken().equals(refreshTokenCookie.getValue())) {
            int userId = 0;
            try {
                userId = JWT.require(Algorithm
                        .HMAC512(KEY.getBytes(StandardCharsets.UTF_8)))
                        .build()
                        .verify(accessToken.replace("Bearer", ""))
                        .getClaim("id")
                        .asInt();
            } catch (Exception e) {
                userId = 0;
            }

            System.out.println("유저네임" + userId);
            if (userId != 0) { // username이 null이 아니라는건 서명이 정상적으로 됬다는 뜻
                User userEntity = userRepository.findById(userId).orElseThrow(() -> {
                    return new IllegalArgumentException("없는 아이디 입니다.");
                });
                ;
                PrincipalDetails principalDetails = new PrincipalDetails(userEntity);
                // Jwt 토큰 서명을 통해서 서명이 정상이면 Authentication 객체를 만들어준다.
                Authentication authentication = new UsernamePasswordAuthenticationToken(principalDetails, null,
                        principalDetails.getAuthorities());
                // 강제로 시큐리티의 세션에 접근하여 값 저장
                response.addHeader("Authorization", accessToken);
                SecurityContextHolder.getContext().setAuthentication(authentication);

            } else { // 검증이 안됬을 때

                System.out.println("액세스토큰만료");
                String refreshToken = refreshTokenCookie.getValue().replace("Bearer", "");
                int ref_userId = 0;
                try {
                    ref_userId = JWT.require(Algorithm.HMAC512(KEY.getBytes(StandardCharsets.UTF_8))).build()
                            .verify(refreshToken)
                            .getClaim("id").asInt();
                } catch (Exception e) {
                    ref_userId = 0;
                }
                if (ref_userId != 0) { // username이 null이 아니라는건 서명이 정상적으로 됬다는 뜻
                    System.out.println("인증완료진짜");
                    User userEntity = userRepository.findById(ref_userId).orElseThrow(() -> {
                        return new IllegalArgumentException("없는 아이디 입니다.");
                    });
                    PrincipalDetails principalDetails = new PrincipalDetails(userEntity);
                    // Jwt 토큰 서명을 통해서 서명이 정상이면 Authentication 객체를 만들어준다.
                    Authentication authentication = new UsernamePasswordAuthenticationToken(principalDetails, null,
                            principalDetails.getAuthorities());
                    // 강제로 시큐리티의 세션에 접근하여 값 저장
                    SecurityContextHolder.getContext().setAuthentication(authentication);

                    String newAccessToken = JWT.create()
                            .withSubject(userEntity.getUsername())
                            .withExpiresAt(new Date(System.currentTimeMillis() + (6000 * 10))) // 토큰만료시간 (1분)
                            .withClaim("id", userEntity.getId())
                            .withClaim("username", userEntity.getUsername())
                            .sign(Algorithm.HMAC512(KEY.getBytes(StandardCharsets.UTF_8)));
                    token.setAccessToken("Bearer" + newAccessToken);
                    tokenRepository.save(token);
                    Constants.OK = true;
                    response.addHeader("Authorization", "Bearer" + newAccessToken);
                }
            }
        } else {
            chain.doFilter(request, response);
            return;
        }
        chain.doFilter(request, response);
    }
}