package com.cos.testspring.config;

import org.springframework.web.filter.CorsFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
public class CorsConfig {

    @Bean
    public CorsFilter corsFilter(){ // CORS 설정 후 시큐리티 필터에 걸어줘야함
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true); // 내 서버가 응답할 떄 json을 자바스크립트에서 처리할 수 있게 할지를 설정하는 것
        config.addAllowedOrigin("http://localhost:3000");// 해당 ip에 접근 허용
        config.addAllowedOrigin("http://localhost:7000");
        config.addAllowedOrigin("http://localhost:6112");
        config.addAllowedOrigin("http://localhost:6221");
        config.addAllowedOrigin("http://localhost:6222");
        config.addAllowedOrigin("http://localhost:6223");// 해당 ip에 접근 허용
        config.addAllowedOrigin("http://192.168.0.232:3000");// 해당 ip에 접근 허용
        config.addAllowedOrigin("http://192.168.0.232:6112");// 해당 ip에 접근 허용
        config.addAllowedOrigin("http://192.168.0.231:3000");// 해당 ip에 접근 허용
        config.addAllowedOrigin("http://192.168.0.231:6112");
        config.addAllowedOrigin("http://192.168.0.239:3000");// 해당 ip에 접근 허용
        config.addAllowedOrigin("http://192.168.0.239:6112");
        config.addAllowedOrigin("http://192.168.0.239:6221");
        config.addAllowedOrigin("http://192.168.0.239:6222");// 해당 ip에 접근 허용
        config.addAllowedOrigin("http://192.168.0.239:6223");
        config.addAllowedOrigin("http://192.168.0.233:6221");
        config.addAllowedOrigin("http://192.168.0.233:6222");// 해당 ip에 접근 허용
        config.addAllowedOrigin("http://192.168.0.233:6223");// 해당 ip에 접근 허용
        config.addAllowedHeader("*"); // 모든 헤더 응답허용
        config.addAllowedMethod("*"); // 모든 방식 응답허용
        config.addExposedHeader("Authorization");//Authorization를 헤더에서 사용할수 있게 허가
        source.registerCorsConfiguration("/**",config); // /** 경로로 오는 모든설정을 config로 하겠다.

        return new CorsFilter(source);
    }
}
