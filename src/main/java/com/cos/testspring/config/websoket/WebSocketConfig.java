package com.cos.testspring.config.websoket;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) { // STOMP에서는 웹소켓처럼 Handler 하나하나 추가할 필요없음
        registry.addEndpoint("/ws").setAllowedOriginPatterns("*").withSockJS(); // addEndpoint => 웹소켓 연결주소
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app"); // 처리나 가공이 필요한 경우
        registry.enableSimpleBroker("/chatroom","/personalChat"); // /chatroom, /personalChat 구독하고 있으면 메시지를 전송할 수 있다.
        // registry.setUserDestinationPrefix("/user"); // 특정유저에게 보내는 사용자 path 지정 , default는 /user임.
    }

    
}
