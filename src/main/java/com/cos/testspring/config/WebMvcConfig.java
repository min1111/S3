package com.cos.testspring.config;

import org.springframework.boot.web.servlet.view.MustacheViewResolver;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer{

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        MustacheViewResolver resolver = new MustacheViewResolver();

        resolver.setCharset("UTF-8");
        resolver.setContentType("text/html;charset=UTF-8");
        resolver.setPrefix("classpath:/templates/");
        resolver.setSuffix(".html");

        registry.viewResolver(resolver);
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:3000")
                .allowedOrigins("http://localhost:7000")// 해당 ip에 접근 허용
                .allowedOrigins("http://localhost:6112")
                .allowedOrigins("http://localhost:6221")
                .allowedOrigins("http://localhost:6222")
                .allowedOrigins("http://localhost:6223")// 해당 ip에 접근 허용
                .allowedOrigins("http://192.168.0.232:3000")// 해당 ip에 접근 허용
                .allowedOrigins("http://192.168.0.232:6112")// 해당 ip에 접근 허용
                .allowedOrigins("http://192.168.0.231:3000")// 해당 ip에 접근 허용
                .allowedOrigins("http://192.168.0.231:6112")
                .allowedOrigins("http://192.168.0.239:3000")// 해당 ip에 접근 허용
                .allowedOrigins("http://192.168.0.239:6112")
                .allowedOrigins("http://192.168.0.239:6221")
                .allowedOrigins("http://192.168.0.239:6222")// 해당 ip에 접근 허용
                .allowedOrigins("http://192.168.0.239:6223")
                .allowedOrigins("http://192.168.0.233:6221")
                .allowedOrigins("http://192.168.0.233:6222")// 해당 ip에 접근 허용
                .allowedOrigins("http://192.168.0.233:6223")
                .allowedMethods("GET","POST") // http 모든 메소드 요청 허용
                .allowedHeaders("*")
                .exposedHeaders("Authorization")
                .allowCredentials(true);
    }
}