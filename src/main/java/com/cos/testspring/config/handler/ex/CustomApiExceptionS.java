package com.cos.testspring.config.handler.ex;

import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.SignatureVerificationException;

public class CustomApiExceptionS extends SignatureVerificationException {

	private static final long serialVersionUID = -807520916259076805L;


	public CustomApiExceptionS(Algorithm algorithm) {

		super(algorithm);
	}
}
