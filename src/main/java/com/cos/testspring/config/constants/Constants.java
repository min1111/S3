package com.cos.testspring.config.constants;

import java.io.Serializable;


public class Constants  implements Serializable {

   /** <code>serialVersionUID</code> : */
   private static final long serialVersionUID = -3831571020204650046L;
   
   // 요청성공
   public static boolean OK = false; 

   public static int MAX_LOGIN_USER = 2; 
   public static final String IMG_PATH ="C:\\Users\\USER\\Desktop\\fileupload\\";//윈도우
   // public static final String IMG_PATH ="/home/min/upload/";//리눅스
}