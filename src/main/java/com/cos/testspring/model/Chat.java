package com.cos.testspring.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "chat")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Builder
public class Chat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 번호증가전략이 데이터베이스를 따라간다..s
    private int id;

    private String userEmail;
    private String message;

    @JsonIgnoreProperties({"chat"})
    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="roomId") // 데이터베이스에 어떤이름으로 만들어질것인지
    private Room room;
}
