package com.cos.testspring.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Builder
@Table(name="board")
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class Board { // 1

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    private String content;
    private String classification;

    public Board(String title,String content,String classification,int userId){
        this.title=title;
        this.content=content;
        this.classification=classification;
        this.userId=userId;
    }
    @JsonIgnoreProperties({"board"})
    @OneToMany(mappedBy="board") 
	private List<img> imgs;

    @JsonIgnoreProperties({"board"})
    @OneToMany(mappedBy="board") 
	private List<file> files;
    
    @JsonIgnoreProperties({"board"})
    @OneToMany(mappedBy="board",fetch = FetchType.EAGER) 
	private List<video> videos;

    private int userId;
	
}
