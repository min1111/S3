package com.cos.testspring.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Id;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.time.LocalDateTime;

import javax.persistence.*;

@Table(name="hydrogenSensor")
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@DynamicInsert 
@DynamicUpdate
public class HydrogenSensor {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="hydrogenId")
    private Hydrogen Hydrogen;//충전소 관리 번호
    
    private LocalDateTime lastMdfcnDt;//최근 수정일
    @ColumnDefault("''")
    private String wait_vhcle_alge="";//현재 대기차 대수
    @ColumnDefault("''")
    private String cnf_sttus_cd="";//혼잡상태코드
    // 0:정보없음,1:여유, 2:보통, 3.혼잡
    // *여유(대기차량 0~1대),
    // 보통(대기차량 2~4대),
    // 혼잡(대기차량 5대이상
    
    private int cumulative_sales;//판매량
}
