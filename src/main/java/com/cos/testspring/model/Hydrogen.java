package com.cos.testspring.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Id;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Table(name="hydrogen")
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@DynamicInsert 
@DynamicUpdate
public class Hydrogen {
    
    @Id
    private String chrstn_mno;//충전소 관리 번호
    @ColumnDefault("''")
    private String chrstn_nm;//충전소 명
    @ColumnDefault("''")
    private String chrstn_cttpc;//충전소 연락처
    @ColumnDefault("''")
    private String road_nm_addr;//충전소 도로명 주소
    @ColumnDefault("''")
    private String lotno_addr; //충전소 지번주소
    @ColumnDefault("''")
    private String cmpt_yn; //충전소 전산여부
    @ColumnDefault("''")
    private String chrstn_ty_cd; 
    //충전소 유형코드 
    //01: 단독, 02 : 복합, 03 : 융합
    @ColumnDefault("''")
    private String chrstn_ty_nm;//충전소 유형명
    @ColumnDefault("''")
    private String chrstn_ty_rm;//충전기 유형비교
    @ColumnDefault("''")
    private String echrgeqp_ty_cd;//충전설비유형코드
    @ColumnDefault("''")
    private String echrgeqp_ty_nm;//충전설비유형명
    @ColumnDefault("''")
    private String chrgr_ty_cd;
    @ColumnDefault("''")
    private String chrgr_ty_nm;
    @ColumnDefault("''")
    private String spldmd_mn_mthd_cd;//충전소 수급방식코드
    // 01:배관,
    // 02:튜브트레일러
    // 03:기타
    @ColumnDefault("''")
    private String spldmd_mn_mthd_nm;// 충전소 수급방식명
    @ColumnDefault("''")
    private String event_cn; // 이벤트 문구
    @ColumnDefault("''")
    private String vhcle_knd_cd; //차량종류코드
    // 01 : 승용차
    // 02 : 버스
    // 03 : 트럭
    // 04 : 특수
    // 05 : 일반,버스
    // 06 : 일반,버스,트럭
    @ColumnDefault("''")
    private String vhcle_knd_nm;//차량 종류명
    @ColumnDefault("0")
    private Integer ntsl_pc;// 판매가격
    @ColumnDefault("''")
    private String setle_mthd_cd;//결제방식코드
//  01 : 카드
//  02 : 현금
//  03 : 현금/카드
//  04 : 협의
    @ColumnDefault("''")
    private String setle_mthd_nm;//결제 방식 명
    @ColumnDefault("''")
    private String use_posbl_dotw;//이용가능요일
    // (1:이용/0:불가)
    // 월화수목금토일공
    // (11111000)
    @ColumnDefault("''")
    private String usebhr_hr_mon;//영업 시작 시간 (월)
    @ColumnDefault("''")
    private String useehr_hr_mon;//영업 종료 시간(월)
    @ColumnDefault("''")
    private String usebhr_hr_tues;//영업 시작 시간 (화)
    @ColumnDefault("''")
    private String useehr_hr_tues;//영업 종료 시간(화)
    @ColumnDefault("''")
    private String usebhr_hr_wed;//영업 시작 시간 (수)
    @ColumnDefault("''")
    private String useehr_hr_wed;//영업 종료 시간(수)
    @ColumnDefault("''")
    private String usebhr_hr_thur;//영업 시작 시간 (목)
    @ColumnDefault("''")
    private String useehr_hr_thur;//영업 종료 시간(목)
    @ColumnDefault("''")
    private String usebhr_hr_fri;//영업 시작 시간 (금)
    @ColumnDefault("''")
    private String useehr_hr_fri;//영업 종료 시간(금)
    @ColumnDefault("''")
    private String usebhr_hr_sat;//영업 시작 시간 (토)
    @ColumnDefault("''")
    private String useehr_hr_sat;//영업 종료 시간(토)
    @ColumnDefault("''")
    private String usebhr_hr_sun;//영업 시작 시간 (일)
    @ColumnDefault("''")
    private String useehr_hr_sun;//영업 종료 시간(일)
    @ColumnDefault("''")
    private String usebhr_hr_hldy;//영업 시작 시간 (공휴일)
    @ColumnDefault("''")
    private String useehr_hr_hldy;//영업 종료 시간(공휴일)
    @ColumnDefault("''")
    private String rest_bgng_hr;//휴식 시작시간
    @ColumnDefault("''") 
    private String rest_end_hr;//휴식 종료시간
    @ColumnDefault("''")
    private String rsvt_posbl_yn;//예약가능여부 Y : 예약가능, N: 예약불가
    @ColumnDefault("''")
    private String lon;//경도
    @ColumnDefault("''")
    private String let;//위도
    @ColumnDefault("''")
    private String oper_yn;//운영여부
    @ColumnDefault("''")
    private String del_at;
    @ColumnDefault("''")
    private String last_mdfcn_dt="";//최근 수정일
    @ColumnDefault("''")
    private String timestamp;//요청시간
    @ColumnDefault("''")
    private String tt_pressr="";// 수소 잔여량
    @ColumnDefault("''")
    private String prfect_elctc_posbl_alge="";//완충 가능대수
    @ColumnDefault("''")
    private String wait_vhcle_alge="";//현재 대기차 대수
    @ColumnDefault("''")
    private String cnf_sttus_cd="";//혼잡상태코드
    // 0:정보없음,1:여유, 2:보통, 3.혼잡
    // *여유(대기차량 0~1대),
    // 보통(대기차량 2~4대),
    // 혼잡(대기차량 5대이상
    @ColumnDefault("''")
    private String cnf_sttus_nm="";//운영상태코드
    // 10:영업정지
    // 20:영업마감
    // 30:운영중
    @ColumnDefault("''")
    private String oper_sttus_cd="";//운영상태 명
    @ColumnDefault("''")
    private String oper_sttus_nm="";//pos 상태코드
    // 0:영업중
    // 1:영업중지
    // 2:점검중
    // 3:T/T교체중
    // 9:기타고장
    @ColumnDefault("''")
    private String pos_sttus_cd="";//영업중
    @ColumnDefault("''")
    private String pos_sttus_nm="";//운영상태 갱신일자

    @ColumnDefault("100")
    private int cumulative_sales;

    private int dangerous;

    public void set(Hydrogen h){
        this.tt_pressr =h.getTt_pressr();
        this.prfect_elctc_posbl_alge=h.getPrfect_elctc_posbl_alge();
        this.wait_vhcle_alge=h.getWait_vhcle_alge();
        this.cnf_sttus_cd=h.getCnf_sttus_cd();
        this.cnf_sttus_nm=h.getCnf_sttus_nm();
        this.oper_sttus_cd=h.getOper_sttus_cd();
        this.oper_sttus_nm=h.getOper_sttus_nm();
        this.pos_sttus_cd=h.getPos_sttus_cd();
        this.pos_sttus_nm=h.getPos_sttus_nm();
        this.last_mdfcn_dt=h.getLast_mdfcn_dt();
    }
}
