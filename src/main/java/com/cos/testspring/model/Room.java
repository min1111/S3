package com.cos.testspring.model;


import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "room")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Builder
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    private int id;

    private String roomName;
    private String opponent;
    private String opponentName;

    @JsonIgnoreProperties({"room"})
    @OneToMany(mappedBy = "room",fetch = FetchType.EAGER)
    List<Chat> chats;

    private LocalDateTime createdAt;


    
}


