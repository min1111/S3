package com.cos.testspring.model;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name="token")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String refreshToken;

    private String accessToken;

    @JsonIgnoreProperties({"token"})
    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="userId")
    private User user;

    private LocalDateTime createDate;
}