package com.cos.testspring.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import javax.persistence.*;

@Table(name="sales")
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class Sales {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int quantity;
    private LocalDate date;

    public Sales(String name,int quantity,LocalDate date){
        this.name=name;
        this.quantity=quantity;
        this.date=date;
    }
}
