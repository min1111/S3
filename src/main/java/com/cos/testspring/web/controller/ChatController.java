package com.cos.testspring.web.controller;


import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.cos.testspring.model.Chat;
import com.cos.testspring.model.Room;
import com.cos.testspring.model.User;
import com.cos.testspring.repository.ChatRepository;
import com.cos.testspring.repository.MessageRepository;
import com.cos.testspring.repository.RoomRepository;
import com.cos.testspring.repository.UserRepository;
import com.cos.testspring.web.dto.ChatDto;
import com.cos.testspring.web.dto.ChatRespDto;

@RequiredArgsConstructor
@Controller
public class ChatController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    private final MessageRepository messageRepository;
    private final RoomRepository roomRepository;
    private final ChatRepository chatRepository;
    private final UserRepository userRepository;
    


   
    /*========= 채팅방생성 =========*/
    @MessageMapping("/firstContactChat") 
    @SendTo("/personalChat/chat")
    public ChatRespDto<?> firstContactChat(ChatDto u){

        System.out.println("---------------firstContactChat---------------");


        Room checkRoom = null;
        
        String RoomName = u.getReceiverName()+","+u.getSenderName();   
        // String RoomName2 = u.getSenderName()+","+u.getReceiverName();

        // checkRoom =roomRepository.findByRoomName(RoomName1);
        // if(checkRoom==null){
        //     checkRoom =roomRepository.findByRoomName(RoomName2);
        // }         

        String[] firstRoomNames = RoomName.split(","); // 방 생성시의 이름
        int size = firstRoomNames.length;
        int count =0;
        
        
        List<Room> originRoomNames = roomRepository.findAll(); // DB에 있는 모든 방 가져오기

        if(originRoomNames != null){//룸이 있는지 없는지 확인
            

                for(Room originRoomName: originRoomNames){ // DB에 있는 모든 방 순환

                    String[] originSplit = originRoomName.getRoomName().split(","); // 각 방의 유저이름을 배열에 담음
                    count=0;
                    for(int i=0; i<originSplit.length;i++){ // 각 방의 유저이름 갯수만큼 반복
                        
                        for(String firstRoomName : firstRoomNames){ // 각방의 유저이름과 내가 만든방의 유저이름 비교
                            if(originSplit[i].equals(firstRoomName)){
                                count++;
                            }
                        }
                    }
                    if(count==size){  // 각방의 유저이름 = 내가만든방의 유저이름 일치하는게 나올 시
                        checkRoom = originRoomName; // 해당 DB의 방을 담아준다.
                    }
                }
        }

        System.out.println(size+"사이즈");
        System.out.println(count+"카운트");

        if(checkRoom!=null){
            return new ChatRespDto<>("기존채팅방 입장",checkRoom.getId()); // 그 방의 방 번호를 준다.
        }else{

        System.out.println("-------------------------");

        //빈방생성
        Room room = new Room(); 
        room.setCreatedAt(LocalDateTime.now());
        room.setRoomName(u.getReceiverName()+","+u.getSenderName());

        
        roomRepository.save(room);


        Chat c1 = new Chat();
        Chat c2 = new Chat();
        c1.setUserEmail(u.getReceiverName());
        c1.setMessage(u.getReceiverName()+"님이 입장하셨습니다.");
        c1.setRoom(room);


        c2.setUserEmail(u.getSenderName());
        c2.setMessage(u.getSenderName()+"님이 입장하셨습니다.");
        c2.setRoom(room);
        
       

        chatRepository.save(c1);
        chatRepository.save(c2);


        getChatList(u.getReceiverName()); // 채팅방 생성과 동시에 상대방의 채팅목록에도 동시생성
        
        return new ChatRespDto<>("채팅방생성",room.getId());  // 새로 생성된 방의 번호를 준다.
        }

        
        //List<Chat> chatList = new ArrayList();
        // for(int i=0;i<u.getUserEmail().size();i++){
        //     Chat c = new Chat();
        //     c.setUserEmail(u.getUserEmail().get(i));
        //     c.setRoom(room);
        //     chatRepository.save(c);
        //    // chatList.add(c);
        // }

        
    }

    /*========= 방번호로 해당 채팅방 입장 =========*/
    @MessageMapping("/getRooms/{roomId}") 
    @SendTo("/personalChat/chat")
    public ChatRespDto<?> getRooms(@DestinationVariable int roomId){

        System.out.println("챗방번호!");
        
        Room r = roomRepository.findById(roomId);

        List<Chat> c = r.getChats();
            
        return new ChatRespDto<>("채팅방입장",c);           
    }


    /*========= 채팅 방 목록 불러오기 =========*/
    @MessageMapping("/getChatList/{userEmail}") 
    @SendTo("/personalChat/chat")
    public ChatRespDto<?> getChatList(@DestinationVariable String userEmail){

        System.out.println("채팅목록!");

 

        List<Room> r = roomRepository.mRoomId(userEmail); // 해당 유저가 있는 방 모두 골라옴

        for(Room oneRoom : r){
            String[] StringRoomName = oneRoom.getRoomName().split(",");

            for(int i=0; i<StringRoomName.length;i++){
                if(!StringRoomName[i].equals(userEmail)){
                    
                    oneRoom.setOpponent(StringRoomName[i]);

                    System.out.println("---------------------------------------");
                    
                    User user = userRepository.findByEmail(StringRoomName[i]);
                    oneRoom.setOpponentName(user.getUsername());

                    roomRepository.save(oneRoom);
                }
            }
        }
         
        return new ChatRespDto<>("채팅목록",r);           
    }


    /*========= 방에서 채팅 입력시 =========*/
    @MessageMapping("/postChat/{roomId}") 
    @SendTo("/personalChat/chat")
    public ChatRespDto<?> postChat(@DestinationVariable int roomId,Chat chat){

        System.out.println("채팅입력!");
        System.out.println(roomId);

        Room room = roomRepository.findById(roomId);

        System.out.println("확인!");

        chat.setRoom(room);

        chatRepository.save(chat);
            
        return new ChatRespDto<>("채팅입력시",chat);           
    }



    
}
 