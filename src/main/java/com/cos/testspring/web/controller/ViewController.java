package com.cos.testspring.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@RequiredArgsConstructor
@Controller
public class ViewController {

    /*==========인덱스==========*/
    @GetMapping("/test")
    public String index(){

        return "temp";

    }


}
