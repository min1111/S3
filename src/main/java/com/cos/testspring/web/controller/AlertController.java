package com.cos.testspring.web.controller;

import com.cos.testspring.model.Message;
import com.cos.testspring.model.User;
import com.cos.testspring.repository.MessageRepository;
import com.cos.testspring.repository.UserRepository;
import com.cos.testspring.web.dto.SocketDto;

import lombok.RequiredArgsConstructor;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;


// 1. 클라이언트에서 메세지를 보내면 일단 먼저 @MessageMapping이 붙은 Controller가 받는다.
// 2. /topic을 destination 헤더로 넣어서 메세지를 바로 송신할 수 도 있고
// 3. /app을 destination 헤더로 넣어서 서버 내에서 가공을 줄 수 있음
// 4. 서버가 가공을 완료했으면, 메시지를 /topic이란 경로를 담아서 다시 전송하면
// 5. SimpleBroker에게 전달되고
// 6. simpleBroker는 전달받은 메세지를 구독자들에게 최종적으로 전달하게 됨.

@RequiredArgsConstructor
@Controller
public class AlertController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    private final MessageRepository messageRepository;
    
    private final UserRepository userRepository;
    // /app/message가 컨트롤러처럼 받아서
    // /chatroom/public 에게 보내준다.


    /*========= 메시지 전송 =========*/
     /*========= 메시지 전송 =========*/
     @MessageMapping("/message") // app과 매치된다 /app/message 이런식으로.
     @SendTo("/chatroom/public")
     public  SocketDto<?> receiveMessage(Message message){
         
         System.out.println("메시지임"+message.getMessage());
         Message myMessage = null;
         /*전체 메시지전송 */
         List<User> users = userRepository.findAll();
         for(User user : users){
            Message m = new Message();
            m.setReceiverName(user.getEmail());
            m.setMessage(message.getMessage());
            messageRepository.save(m);
            myMessage = m;
         }
        // // System.out.println("principal : "+principal.getUser().getId());
        // System.out.println("오류났다");
        //  long count= messageRepository.countByAlertAndReceiverName(0,"min2@naver.com"); // Alert가 0 (안읽음)의 카운트를 보내준다.
        //  System.out.println("오류안났다");
        //  int RealCount = (int)count;       
 
         return new SocketDto<>(1,0,myMessage);
     }

    /*========= 벨 클릭시 =========*/
    @MessageMapping("/bell") 
    @SendTo("/chatroom/public")
    public SocketDto<?> bellMessage(Message message){
        

        if(message.getToggle()){ // 종을 처음클릭시 1로 들어옴

            List<Message> m = messageRepository.findByReceiverName(message.getReceiverName()); // 해당유저의 메시지 다받아옴

            for(Message m1 : m){    // Alert를 1로 변경해줌으로써 읽었다는 표시
               m1.setAlert(1);  
               messageRepository.save(m1);       
            }
            return new SocketDto<>(2,0,m);            
        }else{ // 종닫음
            
            return new SocketDto<>(2,(int)(messageRepository.countByAlertAndReceiverName(0,message.getReceiverName())) ,null);       
        }

    }         
    
    /*========= 카운팅 전송 =========*/
    @MessageMapping("/count") 
    @SendTo("/chatroom/public")
    public SocketDto<?> countMessage(Message message){
        
        
        long count=messageRepository.countByAlertAndReceiverName(0,message.getReceiverName()) ; // Alert가 0 (안읽음)의 카운트를 보내준다.
        int RealCount = (int)count;       

        return new SocketDto<>(3,RealCount,null);            
    }

    // 클라이언트에서 /app/private-message 경로로 메시지를 보내는 요청을 하면
    // 메시지 Controller가 받아서 /private을 구독하고 있는 클라이언트에게 메시지를 전달한다.
    @MessageMapping("/private-message")
    public Message recMessage(@Payload Message message){

        simpMessagingTemplate.convertAndSendToUser(message.getReceiverName(),"/private",message); // private을 구독
        System.out.println(message.toString());
        return message;
    }


    
}
 