package com.cos.testspring.web.filter;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class MyFilter1 implements Filter { // Filter를 implements해주면 필터가 된다.


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        //토큰을 "cos"라고 가정
        //id,pw정상적으로 들어와서 로그인이 완료되면 토큰을 만들어주고 그걸 응답해준다.
        //요청할 때 마다 header에 Authoriztion에 value값으로 토큰을 가지고 온다.
        //그 때 토큰이 넘어오면 이 토큰이 내가 만든 토큰이 맞는지만 검증해주면 됨. (RSA 또는 HS256 으로 검증해주면됨)
        if(req.getMethod().equals("POST")){ // POST요청일 떄만 실행
            String headerAuth = req.getHeader("Authorization"); // header로 부터 Authorization을 받아오겠다.
            System.out.println(headerAuth+"어쓰입니다");

            if(headerAuth.equals("cos")){ // 넘어온 토큰이 cos일 때만 chain을 타게하겠다.
                chain.doFilter(req,res);
            }else{
                PrintWriter out = res.getWriter();
                out.println("not token");
            }
        }



        System.out.println("필터11");
      //  chain.doFilter(request,response); // 이 프로세스를 계속 진행하려면 chain에다가 필터를 이렇게 넘겨주어야함.

    }
}
