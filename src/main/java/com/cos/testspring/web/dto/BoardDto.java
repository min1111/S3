package com.cos.testspring.web.dto;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cos.testspring.model.Board;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BoardDto {

	//private MultipartFile file; // 파일은 MultipartFile을 통해 받을 수 있음
	private String title;
	private String content;
	private String classification;
	private List<MultipartFile> file; // 파일은 MultipartFile을 통해 받을 수 있음
	private List<MultipartFile> img;
	private List<MultipartFile> video;
	public Board toEntity(String classification,int user) {
		return Board.builder()
				.title(title)
				.content(content)
				.classification(classification)
				.userId(user)
				.build();
	}
}
