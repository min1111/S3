package com.cos.testspring.web.dto;

import com.cos.testspring.config.constants.Constants;

import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor
@Data
public class loginBody {

    public Constants constants;
    private String role;
	private String email;
    private Boolean login = false;
    private Boolean newTokenCheck=Constants.OK;
    public loginBody(String role,String email,Boolean login){
        super();
        this.role = role;
        this.email = email;
        this.login = login;
    }
}
