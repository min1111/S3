package com.cos.testspring.web.dto;


import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ChatRespDto<T> {
	
	private String stringCode; 	
	private T result;
}
