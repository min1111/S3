package com.cos.testspring.web.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatDto {
    //public List<String> userEmail;
    public String senderName;
    public String receiverName;
    public String message;
}
