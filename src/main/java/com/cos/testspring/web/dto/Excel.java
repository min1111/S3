package com.cos.testspring.web.dto;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.jpa.repository.JpaRepository;

import com.cos.testspring.service.Service;

import lombok.Data;
@Data
public class Excel<S extends Service<T>,T,R extends JpaRepository>{
    //T 타입을 제네릭으로 사용중인 서비스를 상속중인 클래스만 사용가능
    //JpaRepository를 상속받은 클래스만 사용가능
    private S service;

    private List<T> tableList;

    private T table;

    private R repository;
    
    public Excel(){}

    //제네릭 안에있는 List테이블에 엑셀에서 읽어온 각행을 넣어준다.
    public void save(Row row,int[] index){ 
        tableList.add(service.save(row, index));
    }

    public int[] getColumn(Row row){
        return service.getColumn(row);
    }
 
    public File toExcel(){
        List<String> nameList = new ArrayList<>();
        try {
            T header = tableList.get(0);
            Field[] fields = header.getClass().getDeclaredFields();//첫 행을 가져와서 
            for(Field field : fields) {//들어있는 값을 확인해 그 수만큼 컬럼을 만들어준다.
                field.setAccessible(true); // 멤버변수의 값에 접근할 수 있게 해줌
                if(field.get(header)!=null){//빈값인지 확인하고
                    if(field.get(header) instanceof Integer || field.get(header) instanceof Double){
                        //타입이 숫자형인경우에는 기본값인 0, 0.0인지 확인함
                        if((field.get(header)+"").equals("0") || (field.get(header)+"").equals("0.0")){
                        }else{
                            nameList.add(field.getName());
                        }
                    }else{
                        nameList.add(field.getName());
                    }
                }
            }
            try (Workbook workbook = new XSSFWorkbook()) {// .xlsx 파일 생성
                Sheet sheet = workbook.createSheet(tableList.get(0).getClass().getSimpleName());//시트 생성및 이름지정
                int rowNo = 0; //행 번호 
                CellStyle headStyle = workbook.createCellStyle(); // 첫번째 행 스타일 지정
                headStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.LIGHT_BLUE.getIndex());//셀배경색
                headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                Font font = workbook.createFont();//첫번째 행 글자 스타일 지정
                font.setColor(HSSFColor.HSSFColorPredefined.WHITE.getIndex());//폰트색상
                font.setFontHeightInPoints((short) 13);//폰트사이즈
                headStyle.setFont(font);
                Row headerRow = sheet.createRow(rowNo++); //첫번째 행 데이터 입력 및 스타일 적용
                for(int i =0;i<nameList.size();i++){
                    headerRow.createCell(i).setCellValue(nameList.get(i));
                    headerRow.getCell(i).setCellStyle(headStyle);
                }
                //db데이터 자동 입력
                for (int i =0;i<tableList.size();i++) {//전채 데이터의 수만큼 반복
                    Row row = sheet.createRow(rowNo++);
                    T body = tableList.get(i);
                    for(int j=0;j<nameList.size();j++){
                        Field field = body.getClass().getDeclaredField(nameList.get(j));
                        field.setAccessible(true);//멤버변수의 값에 접근할 수 있게 해줌
                        row.createCell(j).setCellValue(field.get(body).toString());
                    }
                }
                //Temp파일 생성 후 만든 엑셀파일을 저장하고 리턴해준다.
                File tmpFile = File.createTempFile("TMP~", ".xlsx");
                try (OutputStream fos = new FileOutputStream(tmpFile);) {
                    workbook.write(fos);
                }
                return tmpFile;
            
        }catch(Exception e) {
            return null;
        }
        }catch(Exception e){
            return null;
        }
    }
}



