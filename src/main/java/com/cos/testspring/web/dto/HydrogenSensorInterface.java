package com.cos.testspring.web.dto;

public interface HydrogenSensorInterface {
    Integer getA();
    Integer getB();
    Integer getC();
    Integer getSum();
}
