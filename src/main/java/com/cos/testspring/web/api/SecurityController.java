package com.cos.testspring.web.api;

import java.time.LocalDateTime;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cos.testspring.config.constants.Constants;
import com.cos.testspring.model.Token;
import com.cos.testspring.model.User;
import com.cos.testspring.repository.TokenRepository;
import com.cos.testspring.repository.UserRepository;
import com.cos.testspring.web.dto.CMRespDto;
import com.cos.testspring.web.dto.loginBody;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
public class SecurityController {
    public Constants constants = new Constants();
    final TokenRepository tokenRepository;
    final UserRepository userRepository;

    @GetMapping("/createToken/{email}/{role}/{accessToken}/{refreshToken}/{login}")
    public ResponseEntity<?> createToken(HttpServletResponse response, @PathVariable String email,
            @PathVariable String role, @PathVariable String accessToken, @PathVariable String refreshToken,
            @PathVariable Boolean login) {

        if (login) {

            User user = userRepository.findByEmail(email);

            response.addHeader("Authorization", accessToken);

            System.out.println("토큰생성");

            System.out.println(email);
            System.out.println(refreshToken);

            Cookie myCookie = new Cookie("refreshToken", refreshToken);
            myCookie.setMaxAge(240000 * 10);
            myCookie.setPath("/"); // 모든 경로에서 접근 가능 하도록 설정
            myCookie.setHttpOnly(true);
            response.addCookie(myCookie);

            Token token = new Token();
            token.setAccessToken(accessToken);
            token.setRefreshToken(refreshToken);
            token.setUser(user);
            token.setCreateDate(LocalDateTime.now());

            List<Token> tokenList = user.getToken();
            // 중복로그인 3개제한
            if (tokenList.size() > Constants.MAX_LOGIN_USER) {
                //기존 로그인된 사용자의 토큰값을 DB에서 삭제함
                for (int i = 0; i < tokenList.size() - (Constants.MAX_LOGIN_USER); i++) {
                    tokenRepository.deleteById(tokenList.get(i).getId());
                }
                tokenRepository.save(token);
            } else {
                tokenRepository.save(token);
            }
            return new ResponseEntity<>(new CMRespDto<>(1, "로그인 성공", null, null, new loginBody(role, email, login)),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new CMRespDto<>(-1, "로그인 실패", null, null, new loginBody(role, email, login)),
                    HttpStatus.OK);
        }
    }

    @GetMapping("/logout")
    public ResponseEntity<?> logout(HttpServletResponse response, HttpServletRequest request) {
        String accessToken = request.getHeader("Authorization");
        Token token = tokenRepository.findByAccessToken(accessToken);
        if (token != null) {
            System.out.println("있다.");
            Cookie myCookie = new Cookie("refreshToken", null);
            myCookie.setMaxAge(0);
            myCookie.setPath("/"); // 모든 경로에서 접근 가능 하도록 설정
            myCookie.setHttpOnly(true);
            response.addCookie(myCookie);
            tokenRepository.deleteById(token.getId());
            return new ResponseEntity<>(new CMRespDto<>(-1, "로그아웃 성공", null, null, new loginBody()), HttpStatus.OK);
        } else {
            System.out.println("없다.");
            return new ResponseEntity<>(new CMRespDto<>(1, "로그아웃 실패", null, null, new loginBody()), HttpStatus.OK);
        }
    }

    @GetMapping("/MaxLoginUser/{MaxLoginUser}")
    public ResponseEntity<?> logout(HttpServletResponse response, HttpServletRequest request,
            @PathVariable int MaxLoginUser) {

                Constants.MAX_LOGIN_USER = MaxLoginUser - 1;
        return new ResponseEntity<>(new CMRespDto<>(1, "중복로그인수 변경완료", null, null, new loginBody()), HttpStatus.OK);
    }
}
