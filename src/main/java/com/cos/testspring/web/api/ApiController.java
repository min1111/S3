package com.cos.testspring.web.api;

import com.cos.testspring.config.auth.PrincipalDetails;
import com.cos.testspring.config.constants.Constants;
import com.cos.testspring.model.Hydrogen;
import com.cos.testspring.model.Message;
import com.cos.testspring.model.User;
import com.cos.testspring.repository.BoardRepository;
import com.cos.testspring.repository.HydrogenRepository;
import com.cos.testspring.repository.MessageRepository;
import com.cos.testspring.repository.SalesRepository;
import com.cos.testspring.repository.UserRepository;
import com.cos.testspring.service.BoardService;
import com.cos.testspring.service.SalesService;
import com.cos.testspring.web.dto.BoardDto;
import com.cos.testspring.web.dto.CMRespDto;
import com.cos.testspring.web.dto.SocketDto;
import com.cos.testspring.web.dto.Excel;
import com.cos.testspring.web.dto.loginBody;
import lombok.RequiredArgsConstructor;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RequiredArgsConstructor
@RestController
public class ApiController {
    public Constants constants;
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final BoardService boardService;
    private final SalesService salesService;
    private final BoardRepository boardRepository;
    private final SalesRepository salesRepository;
    private final MessageRepository messageRepository;
    private final HydrogenRepository hydrogenRepository;
    public boolean newTokenCheck = false;
    static int index;

    public void setNewTokenCheck(boolean newTokenCheck){
        this.newTokenCheck=newTokenCheck;
    }
    
    @PostMapping("/join")
    public @ResponseBody ResponseEntity<?> join(User user){
        System.out.println("----------");
        System.out.println(user);
        System.out.println(user.getEmail());
        System.out.println("----------");
        if(userRepository.findByEmail(user.getEmail()) != null){
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            return new ResponseEntity<>(new CMRespDto<>(-1,"이미 존재하는 계정입니다.",null,null,new loginBody()),HttpStatus.OK);
        }

        user.setRole("ROLE_USER");

        String rawPassword = user.getPassword();
        String encPassword = bCryptPasswordEncoder.encode(rawPassword);

        user.setUsername(user.getUsername());
        user.setPassword(encPassword);
        user.setProfil("3.jpg");

        userRepository.save(user);
        return new ResponseEntity<>(new CMRespDto<>(1,"회원가입성공",null,null,new loginBody()),HttpStatus.OK);
    }

    
    @GetMapping("/message/{jwtToken}/{email}/{role}/{login}")
    public ResponseEntity<?> messageForHeader(HttpServletRequest request, HttpServletResponse response,@PathVariable String jwtToken,@PathVariable String email,@PathVariable String role,@PathVariable Boolean login) {
        response.addHeader("Authorization", "Bearer"+jwtToken);
        
        if(login){
        return new ResponseEntity<>(new CMRespDto<>(1,"로그인 성공",null,null,new loginBody(role,email,login)),HttpStatus.OK);
        }else {
            return new ResponseEntity<>(new CMRespDto<>(-1,"로그인 실패",null,null,new loginBody(role,email,login)),HttpStatus.OK);
        }
    }

    //user,manager,admin 권한 접근가능
    @GetMapping("/api/v1/user")
    public String user(Authentication authentication) {
        PrincipalDetails principal = (PrincipalDetails) authentication.getPrincipal();
        System.out.println("principal : "+principal.getUser().getId());
        System.out.println("principal : "+principal.getUser().getUsername());
        System.out.println("principal : "+principal.getUser().getPassword());
        return "확인";
    }
    @GetMapping("/api/v1/user/check")
    public ResponseEntity<?> userCheck(Authentication authentication) {
        System.out.println(newTokenCheck);
        PrincipalDetails principal = (PrincipalDetails) authentication.getPrincipal();
        principal.getUser().setPassword(null);
        principal.getUser().setToken(null);
        return new ResponseEntity<>(new CMRespDto<>(1,"유저확인",principal.getUser(),null,new loginBody(null,null,true)),HttpStatus.OK);
    }

    @GetMapping("/exception")
    public ResponseEntity<?> exception(){
        
        System.out.println("컨트롤러 도착");
        return new ResponseEntity<>(new CMRespDto<>(-1,"인증실패",null,null,new loginBody(null,null,false)),HttpStatus.OK);
    }
    @GetMapping("/successfulLogin")
    public ResponseEntity<?> successfulLogin(){

        return new ResponseEntity<>(new CMRespDto<>(1,"로그인 성공",null,null,new loginBody()),HttpStatus.OK);

    }

    // @GetMapping("/startDemo")//센서,중계기 역활
    // public void startDemo(){
    //     //전국 수소충전소 마다 더미데이터를 생성해 서버로 보내줌
    //     //총 151개의 타이머가 생성됨
    //     List<Hydrogen> hydrogenList = hydrogenRepository.findAll();
        
    //     for(index=0;index<hydrogenList.size();index++){
    //         TimerTask task = new TimerTask() {
    //             int a = index;
    //             public void run(){
    //                 System.out.println(a+"번째 타이머 실행");
    //                 BufferedReader in = null;
    //                 Hydrogen hydrogen =hydrogenList.get(a);
    //                 try{
    //                     URL obj = new URL("http://192.168.0.233:6112/demoData/"+hydrogen.getChrstn_mno()+"/"
    //                     +((int)(Math.random()*8))+"/"+((int)(Math.random()*450)+50)); // 호출할 url
    //                     HttpURLConnection con = (HttpURLConnection)obj.openConnection();
    //                     con.setRequestMethod("GET");
    //                     in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
    //                 }catch(Exception e){}
                    
    //             }
    //         };
    //         Timer timer = new Timer("Timer");
    //         long delay = 1000L;//0.02초씩 차이나게 순차적으로 실행
    //         long period = 3000L;//3초마다 실행
    //         timer.scheduleAtFixedRate(task, delay, period);
    //     }
    // }
    @GetMapping("/startDemo")//센서,중계기 역활
    public void startDemo(){
        //전국 수소충전소 마다 더미데이터를 생성해 서버로 보내줌
        //총 151개의 타이머가 생성됨
        List<Hydrogen> hydrogenList = hydrogenRepository.findAll();
        
        for(index=0;index<hydrogenList.size();index++){
            TimerTask task = new TimerTask() {
                int a = index;
                public void run(){
                    System.out.println(a+"번째 타이머 실행");
                    BufferedReader in = null;
                    Hydrogen hydrogen =hydrogenList.get(a);
                    try{
                        URL obj = new URL("http://localhost:6112/demoData/"+hydrogen.getChrstn_mno()+"/"
                        +((int)(Math.random()*8))+"/"+((int)(Math.random()*450)+50)); // 호출할 url
                        HttpURLConnection con = (HttpURLConnection)obj.openConnection();
                        con.setRequestMethod("GET");
                        in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                    }catch(Exception e){}
                    
                }
            };
            Timer timer = new Timer("Timer");
            long delay = 1000L;//0.02초씩 차이나게 순차적으로 실행
            long period = 3000L;//3초마다 실행
            timer.scheduleAtFixedRate(task, delay, period);
        }
    }

    @GetMapping("/api/v1/user/getHydrogen")
    public ResponseEntity<?> getHydrogen(){
        JSONArray totelJsonArray = new JSONArray();
        try{
        List<Hydrogen> hydrogen = hydrogenRepository.findAll();
        for(int i =0;i<hydrogen.size();i++){
            JSONObject totalJsonObject = new JSONObject();
            JSONObject jSONObject = new JSONObject();
            totalJsonObject.put("id", hydrogen.get(i).getChrstn_mno());
            totalJsonObject.put("name", hydrogen.get(i).getChrstn_nm());
            jSONObject.put("lat", hydrogen.get(i).getLet());
            jSONObject.put("lng", hydrogen.get(i).getLon());
            totalJsonObject.put("position", jSONObject);
            totalJsonObject.put("operation", hydrogen.get(i).getOper_sttus_nm());
            totalJsonObject.put("congestion", hydrogen.get(i).getCnf_sttus_cd());
            totalJsonObject.put("price", hydrogen.get(i).getNtsl_pc());
            totalJsonObject.put("number", hydrogen.get(i).getChrstn_cttpc());
            totalJsonObject.put("address", hydrogen.get(i).getLotno_addr());
            totelJsonArray.add(totalJsonObject);
        }
        }catch(Exception e){
            return new ResponseEntity<>(new CMRespDto<>(1,"조회 실패",null,null,new loginBody()),HttpStatus.OK);
        }
        return new ResponseEntity<>(new CMRespDto<>(1,"조회 성공",null,totelJsonArray,new loginBody()),HttpStatus.OK);
    }

    @PostMapping("/adminmessage")
    public SocketDto<?> adminMessage(Message message){
        System.out.println("============================");

        List<Message> m = messageRepository.findByReceiverName(message.getReceiverName());
        System.out.println("============================"+m);

        return new SocketDto<>(0,0,m);      

    }

    //엑셀파일을 형식에 맞춰 넣으면 DB에 저장해줌
    @PostMapping("/excel/read")
    public @ResponseBody ResponseEntity<?> excelRead(BoardDto boardDto) throws IOException{
        String extension = FilenameUtils.getExtension(boardDto.getFile().get(0).getOriginalFilename());
        //파일 확장자만 저장함
        Excel excel = new Excel<>();
        
        Workbook workbook = null;
        
        if (extension.equals("xlsx")) {//보내온 확장자에 맞춰 workbook 생성
        workbook = new XSSFWorkbook(boardDto.getFile().get(0).getInputStream());
        } else if (extension.equals("xls")) {
        workbook = new HSSFWorkbook(boardDto.getFile().get(0).getInputStream());
        }else{
            return new ResponseEntity<>(new CMRespDto<>(-1,"엑셀파일만 업로드 해주세요",null,null,new loginBody()),HttpStatus.OK);
        }

        Sheet worksheet = workbook.getSheetAt(0);
        switch(worksheet.getRow(0).getCell(0).getStringCellValue()){
            case "board":
                excel.setService(boardService);
                excel.setRepository(boardRepository);
                break; 
            case "sales":
                excel.setService(salesService);
                excel.setRepository(salesRepository);
                break; 
            default: 
            return new ResponseEntity<>(new CMRespDto<>(-1,"알맞는 테이블명이 아닙니다.",null,null,new loginBody()),HttpStatus.OK);
        }
        try{
            int[] index = excel.getColumn(worksheet.getRow(1));
            excel.setTableList(new ArrayList());
            for (int i = 2; i < worksheet.getPhysicalNumberOfRows(); i++) { // 4
            Row row = worksheet.getRow(i);
            excel.save(row, index);
            }
        }catch(Exception e){
            return new ResponseEntity<>(new CMRespDto<>(-1, "엑셀 파일 읽기 실패",null,null,new loginBody()),HttpStatus.OK);
        }
        try{
            excel.getRepository().saveAll(excel.getTableList());
        }catch(Exception e){
            return new ResponseEntity<>(new CMRespDto<>(-1,"DB 저장 실패",null,null,new loginBody()),HttpStatus.OK);
        }
        return new ResponseEntity<>(new CMRespDto<>(1,"DB 저장 성공",null,null,new loginBody()),HttpStatus.OK);
    }
}