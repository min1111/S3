package com.cos.testspring.web.api;

import com.cos.testspring.config.auth.PrincipalDetails;
import com.cos.testspring.config.constants.Constants;
import com.cos.testspring.model.Board;
import com.cos.testspring.model.img;
import com.cos.testspring.repository.BoardRepository;
import com.cos.testspring.repository.ImgRepository;
import com.cos.testspring.service.BoardService;
import com.cos.testspring.service.SalesService;
import com.cos.testspring.service.UserService;
import com.cos.testspring.web.dto.BoardDto;
import com.cos.testspring.web.dto.CMRespDto;
import com.cos.testspring.web.dto.loginBody;
import org.springframework.http.MediaType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
@RestController 
public class BoardApiController {
    public Constants constants;
    private final BoardRepository boardRepository;
    private final BoardService boardService;
    private final ImgRepository imgRepository;
    private final UserService userService;
    public boolean newTokenCheck = false;
    private final SalesService salesService;
    public void setNewTokenCheck(boolean newTokenCheck){
        this.newTokenCheck=newTokenCheck;
    }

    @PostMapping("/api/v1/user/posting")
    public @ResponseBody ResponseEntity<?> posting(BoardDto boardDto,Authentication authentication){
        PrincipalDetails principal = (PrincipalDetails) authentication.getPrincipal();
        Board board = boardService.insertPosting(boardDto,principal.getUser()); // DB 저장        
            
        return new ResponseEntity<>(new CMRespDto<>(1,"글 포스팅 성공",board,null,new loginBody()),HttpStatus.OK);
    }
    
    @GetMapping("/api/v1/user/boards/{classification}")
    public ResponseEntity<?> getBoardsByclassification(@PathVariable String classification,
    @PageableDefault(size = 9, sort = "id", direction = Sort.Direction.DESC) Pageable pageable){
        System.out.println("여기 1");
        if(classification.equals("report")){
            classification = "보도자료";
        }else if(classification.equals("media")){
            classification = "미디어행사";
        }else if(classification.equals("all")){
            classification = "all";
        }else{
            classification = "";
        }
        long MaxPage =boardRepository.count();
            if(MaxPage%9 == 0){
                MaxPage = MaxPage/9-1;
            }else {
                MaxPage = MaxPage/9;
            }
            Page<Board> board=null;
        if(classification.equals("")){
            return new ResponseEntity<>(new CMRespDto<>(1,"게시판 불러오기 실패",null,null,new loginBody()),HttpStatus.OK);
        }else if(classification.equals("all")){
            board = boardService.getBoard(pageable);
        }else{
            board = boardService.getBoard(pageable,classification);
        }
        if(board == null){
            return new ResponseEntity<>(new CMRespDto<>(1,"조건에 맞는 게시판 없음",null,null,new loginBody()),HttpStatus.OK);
        }
        return new ResponseEntity<>(new CMRespDto<>(1,"게시판 불러오기 성공",MaxPage,board,new loginBody()),HttpStatus.OK);
    }

    @GetMapping("/api/v1/user/board/{id}")
    public ResponseEntity<?> getBoardById(@PathVariable Integer id){
        Board board =null;
        try{
            board = boardRepository.findById(id).orElseThrow();
            
        }catch(Exception e){
            return new ResponseEntity<>(new CMRespDto<>(1,"게시판 불러오기 실패",null,null,new loginBody()),HttpStatus.OK);
        }
        
        return new ResponseEntity<>(new CMRespDto<>(1,"게시판 불러오기 성공",board,null,new loginBody(null,null,true)),HttpStatus.OK);
    }
    

    @GetMapping("/api/v1/user/addDummy")
    public ResponseEntity<?> addBoard(){
       for(int i=0;i<107;i++) {
        Board board = new Board();
        img img = new img();
          int j =i%5;
          if(j==0){
            board.setTitle("Pacom엔터, 2022년 프리미엄IP 라인업으로 글로벌 스튜디오 도약! 독보적 크리에이티브와 제작 경쟁력으로 세계 시장 정조준");
            board.setClassification("보도자료");
            img.setName("dummyimg1.png");
          }else if(j==1){
            board.setTitle("Pacom, 뉴스 댓글 서비스에 ‘세이프봇’ 적용 성과 공개");
            board.setClassification("보도자료");
            img.setName("dummyimg2.jpg");
          }else if(j==2){
            board.setTitle("Pacom, 개발자 컨퍼런스 'if (kakao) dev 2022’에서 서비스 장애 재발방지 대책 발표");
            board.setClassification("보도자료");
            img.setName("dummyimg3.jpg");
          }else if(j==3){
            board.setTitle("Pacom AI 미디어 스터디 제1강 - 대화엔진(챗봇)");
            board.setClassification("미디어행사");
            img.setName("dummyimg4.jfif");
          }else if(j==4){
            board.setTitle("Pacom AI 미디어 스터디 제2강 - 시각엔진");
            board.setClassification("미디어행사");
            img.setName("dummyimg5.jpg");
          }
          
          board.setUserId(1);
          board.setContent("asdf"+i);
          
          boardRepository.save(board);
          img.setBoard(board);
          imgRepository.save(img);
       }
       salesService.addSales1();
       userService.addDummyUser();
        return new ResponseEntity<>(new CMRespDto<>(1,"더미데이터 생성 성공",null,null,new loginBody()),HttpStatus.OK);
    }
    @GetMapping(
        value = "/img/{imgName}",
        produces = MediaType.IMAGE_JPEG_VALUE
    )
    public void getImageWithMediaType(HttpServletResponse response,@PathVariable String imgName) throws IOException {
        OutputStream out = response.getOutputStream();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(Constants.IMG_PATH+imgName);
            FileCopyUtils.copy(fis, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
            out.flush();
        }
    }
}