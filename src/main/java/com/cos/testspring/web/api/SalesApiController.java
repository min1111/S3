package com.cos.testspring.web.api;

import com.cos.testspring.config.constants.Constants;
import com.cos.testspring.repository.HydrogenRepository;
import com.cos.testspring.repository.SalesRepository;
import com.cos.testspring.service.SalesService;
import com.cos.testspring.web.dto.CMRespDto;
import com.cos.testspring.web.dto.Excel;
import com.cos.testspring.web.dto.SalesDto;
import com.cos.testspring.web.dto.SalesInterface;
import com.cos.testspring.web.dto.loginBody;
import lombok.RequiredArgsConstructor;

import org.json.simple.JSONObject;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
@RestController
public class SalesApiController {
    public Constants constants;
    private final SalesRepository salesRepository;
    private final SalesService salesService;
    private final HydrogenRepository hydrogenRepository;
    
    public boolean newTokenCheck = false;
    public void setNewTokenCheck(boolean newTokenCheck) {
        this.newTokenCheck = newTokenCheck;
    }

    @GetMapping("/api/v1/user/addSales")
    public ResponseEntity<?> addSales() {
       try{
        
        salesService.addSales1();
       }catch(Exception e){
        return new ResponseEntity<>(new CMRespDto<>(1, "더미데이터 생성실패", null, null, new loginBody()), HttpStatus.OK);
       }
        return new ResponseEntity<>(new CMRespDto<>(1, "더미데이터 생성완료", null, null, new loginBody()), HttpStatus.OK);
    }

    @GetMapping("/api/v1/user/progressBar/{date}")
    public ResponseEntity<?> comparison(@PathVariable String date) {
        List<SalesDto> salesDtoList = salesService.comparison(date);
        if(salesDtoList == null){
            return new ResponseEntity<>(new CMRespDto<>(1, "데이터 조회 실패", null, null, new loginBody()),HttpStatus.OK);
        }else{
            return new ResponseEntity<>(new CMRespDto<>(1, "데이터 조회 성공", null, salesDtoList, new loginBody()),HttpStatus.OK);
        }
    }

    @GetMapping("/api/v1/user/pie/{date}")
    public ResponseEntity<?> ratio(@PathVariable String date) {
        List<SalesDto> salesDtoList = salesService.ratio(date);
        if(salesDtoList == null){
            return new ResponseEntity<>(new CMRespDto<>(1, "데이터 조회 실패", null, null, new loginBody()),HttpStatus.OK);
        }else{
            return new ResponseEntity<>(new CMRespDto<>(1, "데이터 조회 성공", null, salesDtoList, new loginBody()),HttpStatus.OK);
        }
    }

    @GetMapping("/api/v1/user/daySelling/{date}")
    public ResponseEntity<?> selectByDay(@PathVariable String date) {
        List<SalesInterface> SalesInterface = null;
        try {
            SalesInterface = salesRepository.findByDay(date);
        } catch (Exception e) {
            return new ResponseEntity<>(new CMRespDto<>(-1, "데이터 조회 실패", null, null, new loginBody()), HttpStatus.OK);
        }
        return new ResponseEntity<>(new CMRespDto<>(1, "데이터 조회 성공", null, SalesInterface, new loginBody()),
                HttpStatus.OK);
    }

    @GetMapping("/api/v1/user/bar/{startDate}/{endDate}")
    public ResponseEntity<?> selectByrange(@PathVariable String startDate, @PathVariable String endDate) {
        List<JSONObject> jsonObjectList =salesService.selectByrange(startDate, endDate);
        if(jsonObjectList ==null){
            return new ResponseEntity<>(new CMRespDto<>(-1, "데이터 조회 실패", null, null, new loginBody()), HttpStatus.OK); 
        }else{
            return new ResponseEntity<>(new CMRespDto<>(1, "데이터 조회 성공", null, jsonObjectList, new loginBody()),HttpStatus.OK);
        }
    }

    @GetMapping("/api/v1/user/salesList/{startDate}/{endDate}")
    public ResponseEntity<?> salesList(@PathVariable String startDate, @PathVariable String endDate) {
        List<JSONObject> salesList = salesService.selectByDayList(startDate, endDate);
        if(salesList ==null){
            return new ResponseEntity<>(new CMRespDto<>(-1, "데이터 조회 실패", null, null, new loginBody()), HttpStatus.OK); 
        }else{
            return new ResponseEntity<>(new CMRespDto<>(1, "데이터 조회 성공", null, salesList, new loginBody()),HttpStatus.OK);
        }
    }

    @GetMapping("/api/v1/user/bump/{startDate}/{endDate}")
    public ResponseEntity<?> selectByrangeRank(@PathVariable String startDate, @PathVariable String endDate) {
        List<JSONObject> jsonObjectList =salesService.selectByrangeRank(startDate, endDate);
        if(jsonObjectList ==null){
            return new ResponseEntity<>(new CMRespDto<>(-1, "데이터 조회 실패", null, null, new loginBody()), HttpStatus.OK); 
        }else{
            return new ResponseEntity<>(new CMRespDto<>(1, "데이터 조회 성공", null, jsonObjectList, new loginBody()),HttpStatus.OK);
        }
    }

    @GetMapping("/excel/{startDate}/{endDate}")
    public ResponseEntity<InputStreamResource> downloadExcel(
        HttpServletResponse response,@PathVariable String startDate, @PathVariable String endDate) throws IOException {
            
        
        File tmpFile = salesService.downloadExcel(startDate, endDate);
            //받아온 File 객체가 가리키는 파일을 쓰기 위한 객체를 생성 후 기존 파일(tempfile)은 삭제해줌
            InputStream res = new FileInputStream(tmpFile) {
                @Override
                public void close() throws IOException {
                    super.close();
                    if (tmpFile.delete()) {
                        System.out.println("삭제완료");
                    }
                }
            };
            //그후 바디에 담아 파일을 보내줌
            return ResponseEntity.ok() //
                    .contentLength(tmpFile.length()) //
                    .contentType(MediaType.APPLICATION_OCTET_STREAM) //
                    .header("Content-Disposition", "attachment;filename=Saleslist.xlsx") 
                    //헤더에 이름을 담음
                    .body(new InputStreamResource(res));
    }
    
    @GetMapping("/excel/download")
    public ResponseEntity<InputStreamResource> downloadExcel2(HttpServletResponse response) throws IOException {
            Excel excel = new Excel();
            
            excel.setTableList(hydrogenRepository.findAll());
        
        File tmpFile = excel.toExcel();
            //받아온 File 객체가 가리키는 파일을 쓰기 위한 객체를 생성 후 기존 파일(tempfile)은 삭제해줌
            InputStream res = new FileInputStream(tmpFile) {
                @Override
                public void close() throws IOException {
                    super.close();
                    if (tmpFile.delete()) {
                        System.out.println("삭제완료");
                    }
                }
            };
            //그후 바디에 담아 파일을 보내줌
            return ResponseEntity.ok() //
                    .contentLength(tmpFile.length()) //
                    .contentType(MediaType.APPLICATION_OCTET_STREAM) //
                    .header("Content-Disposition", "attachment;filename="+excel.getTableList().get(0).getClass().getSimpleName()+".xlsx") 
                    //헤더에 이름을 담음
                    .body(new InputStreamResource(res));
    }
    }
